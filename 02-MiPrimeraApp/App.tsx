import React from 'react';
import {SafeAreaView} from 'react-native';
import { TareaScreen } from './src/screens/TareaScreen';
// import { FlexScreen } from './src/screens/FlexScreen';
// import { PositionScreen } from './src/screens/PositionScreen';
// import {BoxObjectModelSreen} from './src/screens/BoxObjectModelSreen';
//  import { ContadoScreen } from './src/screens/ContadoScreen';
// import { HolaMundoScreen } from './src/screens/HolaMundoScreen';
// import {DimensionesScreen} from './src/screens/DimensionesScreen';


export const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <HolaMundoScreen /> */}
       {/* <ContadoScreen /> */}
      {/* <BoxObjectModelSreen/> */}
      {/* <DimensionesScreen /> */}
      {/* <PositionScreen/> */}
      {/* <FlexScreen></FlexScreen> */}
      <TareaScreen></TareaScreen>
    </SafeAreaView>
  );
};
