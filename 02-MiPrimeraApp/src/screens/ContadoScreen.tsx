import React, {useState} from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import {Fab} from '../components/Fab';

export const ContadoScreen = () => {
  const [contador, setContador] = useState(5);

  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>Contador: {contador}</Text>

      <Fab 
        title="-1" 
        position= "bl"
        onPress={() => setContador(contador - 1)} 
      />

      <Fab 
        title="+1" 
        position= "br"
        onPress={() => setContador(contador + 1)} 
      />

     
    </View>
  );
};

//Definiendo los estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  titulo: {
    fontSize: 35,
    textAlign: 'center',
    top: -10,
  },
  fabLocationBder: {
    position: 'absolute',
    bottom: 30,
    right: 0,
  },
  fabLocationBizq: {
    position: 'absolute',
    bottom: 30,
    left: 0,
  },
  fab: {
    backgroundColor: '#5856D6',
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
  },
  fabText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
