import React from 'react';
import {View, StyleSheet} from 'react-native';

export const TareaScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.cajaMorada}></View>
      <View style={styles.cajaNaranja}></View>
      <View style={styles.cajaAzul}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#28425B',
    justifyContent: 'center',
    alignItems:'center',
    flexDirection:'row'
  },
  cajaMorada: {
    //   flex: 2,
    width: 100,
    height: 100,
    borderWidth: 10,
    borderColor: 'white',
    backgroundColor: '#5856D6',
    // alignSelf:'flex-end'
    // top: 100
  },
  cajaNaranja: {
    // flex: 2,
    width: 100,
    height: 100,
    borderWidth: 10,
    // flex: 1,
    borderColor: 'white',
    backgroundColor: '#F0A23B',
    // alignSelf:'center'
    // left: 100
    top: 50
  },
  cajaAzul: {
    // flex: 4,
    width: 100,
    height: 100,
    borderWidth: 10,
    borderColor: 'white',
    backgroundColor: '#28C4D9',
    // alignSelf:'flex-start',
  },
});
