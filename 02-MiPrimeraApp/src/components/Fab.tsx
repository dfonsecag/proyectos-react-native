import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';

interface Props {
  title: String;
  position?: 'br' | 'bl';
  onPress: () => void;
}

export const Fab = ({title, onPress, position = 'br'}: Props) => {
  return (
    <View
      style={
        position === 'bl' ? styles.fabLocationBizq : styles.fabLocationBder
      }>
      <TouchableNativeFeedback 
      background= { TouchableNativeFeedback.Ripple('black', false,30)}
      onPress={onPress}>
        <View style={styles.fab}>
          <Text style={styles.fabText}>{title}</Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

//Definiendo los estilos
const styles = StyleSheet.create({
  fabLocationBder: {
    position: 'absolute',
    bottom: 30,
    right: 0,
  },
  fabLocationBizq: {
    position: 'absolute',
    bottom: 30,
    left: 0,
  },
  fab: {
    backgroundColor: '#5856D6',
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 2,
  },
  fabText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
