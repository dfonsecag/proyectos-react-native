import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
} from 'react-native';
import { Fecha } from '../components/Fecha';
import { Hora } from '../components/Hora';
import { styles } from '../theme/styles';


export const CrearCita = ({navigation, route}) => {
 
  

  useEffect(() => {
    navigation.setOptions({
      title: 'Crear Cita',
    });
  }, []);

 

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>Nombre mascota:</Text>
        <TextInput
          placeholder="Mascota"
          style={styles.textInput}
          // value={ nombre }
          // onChangeText={ ( value )=> onChange( value, 'nombre' )  }
        />

        <Text style={styles.label}>Nombre del dueño:</Text>
        <TextInput
          placeholder="Dueño"
          style={styles.textInput}
          // value={ nombre }
          // onChangeText={ ( value )=> onChange( value, 'nombre' )  }
        />
        
      <Fecha/>

      <Hora/>
       
      </ScrollView>

      
    </View>


  );
};

