import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Citas } from '../screens/Citas';
import { CrearCita } from '../screens/CrearCita';

const Stack = createStackNavigator();

export const Navigator = () => {
    return (
        <Stack.Navigator>
        <Stack.Screen name="CrearCita" component={CrearCita} />     
        <Stack.Screen name="Citas" component={Citas} />
         
      </Stack.Navigator>
    )
}
