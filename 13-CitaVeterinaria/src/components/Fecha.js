import React, { useState } from 'react';
import { Button, Text } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { styles } from '../theme/styles';

export const Fecha = () => {

  const [isfecha, setIsFecha] = useState(false);

  const showFecha = () => {
    setIsFecha(true);
  };

  const hideFecha = () => {
    setIsFecha(false);
  };

  const handleFecha = data => {
    let dateTimeString =
      data.getDate() +
      '-' +
      (data.getMonth() + 1) +
      '-' +
      data.getFullYear() +
      ' ';

    // return dateTimeString;
    console.warn('La fecha es ', dateTimeString);
    hideFecha();
  };

  return (
    <>
      <Text style={styles.label}>Fecha:</Text>
      <Button title="Fecha" onPress={showFecha} />
      <DateTimePicker
        isVisible={isfecha}
        mode="date"
        onConfirm={handleFecha}
        onCancel={hideFecha}
      />
    </>
  );
};
