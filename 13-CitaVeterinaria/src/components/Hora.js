import React, { useState } from 'react';
import { Button, Text } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { styles } from '../theme/styles';

export const Hora = () => {

  const [ishora, setIsHora] = useState(false);

  const showHora = () => {
    setIsHora(true);
  };

  const hideHora = () => {
    setIsHora(false);
  };

  const handleTime = data => {
    let dateTimeString = data.getHours() + ':' + data.getMinutes();

    // return dateTimeString;
    console.warn('La Hora es ', dateTimeString);
    hideHora();
  };
  return (
    <>
      <Text style={styles.label}>Hora:</Text>
      <Button title="Hora" onPress={showHora} />
      <DateTimePicker
        isVisible={ishora}
        mode="time"
        onConfirm={handleTime}
        onCancel={hideHora}
      />
    </>
  );
};
