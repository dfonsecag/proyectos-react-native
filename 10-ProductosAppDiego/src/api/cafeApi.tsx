import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';

const baseURL = 'http://10.33.0.32:8181/api';

const cafeApi = axios.create({baseURL});

//Midleware para enviar el token
cafeApi.interceptors.request.use(
    async(config)=>{
        const token = await AsyncStorage.getItem('token');
        if(token){
            config.headers['x-token'] = token;
        }
        return config;
    }
)

export default cafeApi;