import { useEffect, useState } from "react"
import cafeApi from "../api/cafeApi";
import { Categoria, CategoriesResponse } from "../interfaces/appInterfaces";


export const useCategorias = () => {


    const [isLoading, setIsLoading] = useState(true);
    const [categorias, setCategorias] = useState<Categoria[]>([])

    useEffect(() => {
       
        getCategorias();
        
    }, [])

    // Funcion para obtener las categorias
    const getCategorias = async() => {
        const resp = await cafeApi.get<CategoriesResponse>('/categorias');
        setCategorias(resp.data.categorias);
        setIsLoading(false);
    }

    return {
        isLoading,
        categorias
    }
}
