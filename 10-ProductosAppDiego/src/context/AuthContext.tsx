import React, {createContext, useEffect, useReducer} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import cafeApi from '../api/cafeApi';
import { Usuario, LoginResponse, LoginData, RegisterData } from '../interfaces/appInterfaces';
import {authReducer, AuthState} from './authReducer';

type AuthContextProps = {
  errorMessage: string;
  token: string | null;
  user: Usuario | null;
  status: 'checking' | 'authenticated' | 'not-authenticated';
  signUp: (registerData: RegisterData) => void;
  signIn: (loginData: LoginData) => void;
  logOut: () => void;
  removeError: () => void;
};

const AuthInitialState: AuthState = {
  status: 'checking',
  token: null,
  user: null,
  errorMessage: '',
};

export const AuthContext = createContext({} as AuthContextProps);

export const AuthProvider = ({children}: any) => {
  const [state, dispatch] = useReducer(authReducer, AuthInitialState);

  useEffect(() => {

    validarToken();

  }, []);

//   Metodo para validar el token
  const validarToken = async() => {

    const token = await  AsyncStorage.getItem('token');
    // Token no autenticado
    if(!token){
        return dispatch({type:'notAuthenticated'});
    }
    // Si hay token
    const resp = await cafeApi.get('/auth');
    if(resp.status !== 200){
        return dispatch({type:'notAuthenticated'});
    }
    
    await AsyncStorage.setItem('token', resp.data.token);
    dispatch({
        type: 'signUp',
        payload: {
          token: resp.data.token,
          user: resp.data.usuario,
        },
      });

    
  };

  // Metodo para autenticar el usuario
  const signIn = async ({correo, password}: LoginData) => {
    try {
      const {data} = await cafeApi.post<LoginResponse>('/auth/login', {correo,password });
      console.log(data);
      dispatch({
        type: 'signUp',
        payload: {
          token: data.token,
          user: data.usuario,
        },
      });

      await AsyncStorage.setItem('token', data.token);
    } catch (error) {
      dispatch({
        type: 'addError',
        payload: error.response.data.msg || 'Informacion incorrecta',
      });
      console.log(error.response.data.msg);
    }
  };

  // Funcion para registrar usuario   
  const signUp = async({nombre, correo, password}: RegisterData) => {
    try {
        const {data} = await cafeApi.post<LoginResponse>('/usuarios', {nombre, correo, password});
        console.log(data);
        dispatch({
          type: 'signUp',
          payload: {
            token: data.token,
            user: data.usuario,
          },
        });
  
        await AsyncStorage.setItem('token', data.token);
      } catch (error) {
        dispatch({
          type: 'addError',
          payload: error.response.data.errors[0].msg || 'Revise la informacion',
        });
       
      }
  };

  //Funcion para cerrar la sesion   
  const logOut = async() => {
    await AsyncStorage.removeItem('token');
    dispatch({type:'logout'});
  };

  //remover el error de login
  const removeError = () => {
    dispatch({type: 'removeError'});
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        signUp,
        signIn,
        logOut,
        removeError,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
