import React, {createContext, useEffect, useState} from 'react';
import { ImagePickerResponse } from 'react-native-image-picker';
import cafeApi from '../api/cafeApi';
import { Producto, ProductsResponse } from '../interfaces/appInterfaces';

type ProductsContextProps = {
  products: Producto[];
  loadProducts: () => Promise<void>;
  addProduct: (categoryId: string, productName: string) => Promise<Producto>;
  updateProduct: (
    categoryId: string,
    productName: string,
    productId: string,
  ) => Promise<void>;
  deleteProduct: (id: string) => Promise<void>;
  loadProductById: (id: string) => Promise<Producto>;
  uploadImage: (data: any, id: string) => Promise<void>; // Todo cambiar Any
};

export const ProductsContext = createContext({} as ProductsContextProps);

export const ProductsProvider = ({children}: any) => {
  const [products, setProducts] = useState<Producto[]>([]);

  // cargar los productos apenas abra la pagina
  useEffect(() => {
    loadProducts()
  }, [])

  //Funcion para cargar Productos
  const loadProducts = async() => {

    const resp = await cafeApi.get<ProductsResponse>('/productos');
    // setProducts([...products, ...resp.data.productos]);
    setProducts([ ...resp.data.productos]);
    console.log(resp.data.productos)

  }
  //Funcion para agregar Producto
  const addProduct = async(categoryId: string, productName: string):Promise<Producto> => {
    console.log('Agregando producto');
    const resp = await cafeApi.post<Producto>('/productos', {
      nombre: productName, 
      categoria:categoryId
    })
    setProducts([...products, resp.data]);

    return resp.data;
  }

  //Funcion para actualizar producto
  const updateProduct = async(categoryId: string,productName: string,productId: string) => {
    console.log('Actualizando Producto');
    const resp = await cafeApi.put<Producto>(`/productos/${productId}`, {
      nombre: productName, 
      categoria:categoryId
    })
    setProducts(products.map(prod => {
      return (prod._id === productId) ? resp.data : prod
    }));
  }

  //Funcion para eliminar un Producto
  const deleteProduct = async(id: string) => {
    const resp = await cafeApi.delete(`/productos/${id}`)
    // Eliminando producto del arreglo
    setProducts(products.filter(prod => prod._id !== id));
  }

  //Funcion para cargar un producto
  const loadProductById = async(id: string):Promise<Producto> => {
    const resp = await cafeApi.get<Producto>(`/productos/${ id }`);
    return resp.data;
   
  }
  //Funcion para cargar la imagen, recordar tipo dato ImagePickerResponse
  const uploadImage = async(data: any, id: string) => {



    const fileToUpload = {
      uri:data.assets[0].uri,
      type: data.assets[0].type,
      name:data.assets[0].fileName
    }

    // resp.assets[0].uri

    const formData = new FormData();
   
    formData.append('archivo', fileToUpload);
    console.log(fileToUpload)

    try {
      
      const resp = await cafeApi.put(`/uploads/productos/${id}`, formData);
      console.log(resp);

    } catch (err) {
      console.log(err.response)
    }

  } 

  return (
    <ProductsContext.Provider
      value={{
        products,
        loadProducts,
        addProduct,
        updateProduct,
        deleteProduct,
        loadProductById,
        uploadImage
      }}>
      {children}
    </ProductsContext.Provider>
  );
};
