import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Alert
} from 'react-native';
import {WhiteLogo} from '../components/WhiteLogo';
import { AuthContext } from '../context/AuthContext';
import { useForm } from '../hooks/useForm';
import {loginStyles} from '../theme/LoginTheme';

interface Props extends StackScreenProps<any,any>{};

export const RegisterScreen = ({navigation}:Props) => {

  const {signUp, errorMessage, removeError} = useContext(AuthContext);

  const {email, password,name, onChange} = useForm({
    name:'',
    email: '',
    password: '',
  });

  useEffect(() => {

    if(errorMessage.length === 0) return;
    
    Alert.alert('Registro Incorrecto', errorMessage, [{text:'Ok', onPress: removeError}]);
     
   }, [errorMessage])

  const onRegister = () => {
    console.log({email, password,name});
    //Quitar el teclado despues de hacer login
    Keyboard.dismiss();
    // llamar al metodo de registrar usuario context
    signUp({
      nombre:name,
      password,
      correo:email
    });
  };

  return (
    <>
      

      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: '#5856d6'}}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <View style={loginStyles.formContainer}>
          {/* Keyboard avoid view */}

          <WhiteLogo />
         
          <Text style={loginStyles.title}>Registro</Text>

           {/* Input name */}
           <Text style={loginStyles.label}>Nombre:</Text>
          <TextInput
            placeholder="Ingrese su nombre:"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIos,
            ]}
            selectionColor="white"
            onChangeText={value => onChange(value, 'name')}
            value={name}
            onSubmitEditing={onRegister}
            autoCapitalize="words"
            autoCorrect={false}
          />

           {/* Input Email */}
          <Text style={loginStyles.label}>Email:</Text>
          <TextInput
            placeholder="Ingrese su email:"
            placeholderTextColor="rgba(255,255,255,0.4)"
            keyboardType="email-address"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIos,
            ]}
            selectionColor="white"
            onChangeText={value => onChange(value, 'email')}
            value={email}
            onSubmitEditing={onRegister}
            autoCapitalize="none"
            autoCorrect={false}
          />
          {/* Input password */}
          <Text style={loginStyles.label}>Password:</Text>
          <TextInput
            placeholder="********"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            secureTextEntry={true}
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIos,
            ]}
            selectionColor="white"
            onChangeText={value => onChange(value, 'password')}
            value={password}
            onSubmitEditing={onRegister}
          />

          {/* Boton de login */}
          <View style={loginStyles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={loginStyles.button}
              onPress={onRegister}>
              <Text style={loginStyles.buttonText}>Crear cuenta</Text>
            </TouchableOpacity>
          </View>
          {/* Ir a login */}
          <TouchableOpacity
            onPress={() => navigation.replace('LoginScreen')}
            activeOpacity={0.8}
            style={loginStyles.buttonReturnLogin}            
          >
            <Text  style={loginStyles.buttonText}>Login</Text>
          </TouchableOpacity>
          
        </View>
      </KeyboardAvoidingView>
    </>
  );
};
