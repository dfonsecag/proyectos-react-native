import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
  Image,
} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {Picker} from '@react-native-picker/picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import {ProductsStackParams} from '../navigator/ProductsNavigator';
import {useCategorias} from '../hooks/useCategorias';
import {useForm} from '../hooks/useForm';
import {ProductsContext} from '../context/ProductsContext';

interface Props
  extends StackScreenProps<ProductsStackParams, 'ProductScreen'> {}

export const ProductScreen = ({route, navigation}: Props) => {
  // parametros
  const {id = '', name = ''} = route.params;
  // State para guadar la imagen temporal que se tomo del dispositivo
  const [tempUri, setTempUri] = useState<String>()
  // todas las categorias
  const {categorias} = useCategorias();
  // metodos del context
  const {loadProductById, addProduct, updateProduct, deleteProduct, uploadImage} = useContext(ProductsContext);
  // Hook use form
  const {_id, categoriaId, nombre, img, form, onChange, setFormValue} = useForm(
    {
      _id: id,
      categoriaId: '',
      nombre: name,
      img: '',
    },
  );
  
  // Para cargar el nombre del producto en el header
  useEffect(() => {
    navigation.setOptions({
      title: nombre ? nombre : 'Sin nombre producto',
    });
  }, [nombre]);

  // Para cargar el articulo que se seleccciono en la pantallla ProductsScreen
  useEffect(() => {
    loadProduct();
  }, []);

  // Carga un producto por Id
  const loadProduct = async () => {
    if (id.length === 0) return;

    const product = await loadProductById(id);
    setFormValue({
      _id: id,
      categoriaId: product.categoria._id,
      img: product.img || '',
      nombre: nombre,
    });
  };

  // Para mandar a llamar la funcion de crear o actualizar el producto
  const saveOrUpdate = async() => {
    if (id.length > 0) {
        updateProduct(categoriaId, nombre, id);
    } 
    else {

        const tempCategoriaId = categoriaId || categorias[0]._id;
        const newProduct = await addProduct(tempCategoriaId,nombre);
        onChange( newProduct._id, '_id');
    }
    navigation.navigate('ProductsScreen');
  };
  
  // Funcion para eliminar Producto
  const deleteProducto = async() => {
    if (id.length > 0) {
      await deleteProduct(id);
      navigation.navigate('ProductsScreen');
    } 
  }
  
  // Funcion para tomar una foto del dispositivo 
  const takePhoto = () => {
    launchCamera({
      mediaType: 'photo',
      quality: 0.5
    }, (resp) => {
      // Para saber si cancelo al tomar la foto
       if( resp.didCancel) return;
      // validar si en la respuesta tenemos el uri
       if(!resp.assets[0].uri) return;

       setTempUri( resp.assets[0].uri );
      //  Mandar a llamar la funcion de subir imagen
      uploadImage(resp, _id);
      // console.log(resp.assets[0].uri);
      // console.log(resp.assets[0].fileName);
    })
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>Nombre del producto:</Text>
        <TextInput
          placeholder="Producto"
          value={nombre}
          onChangeText={value => onChange(value, 'nombre')}
          style={styles.textInput}
        />

        {/* Picker o selector */}
        <Text style={styles.label}>Seleccione la categoria:</Text>
        <Picker
          selectedValue={categoriaId}
          onValueChange={value => onChange(value, 'categoriaId')}>
          {categorias.map(c => (
            <Picker.Item label={c.nombre} value={c._id} key={c._id} />
          ))}
        </Picker>

        {/* boton de envio formulario */}
        <Button title="Guardar" onPress={saveOrUpdate} color="#5856D6" />
        
        
       
        {_id.length > 0 && (
          <>
          <View style={{marginTop:10}}>
          <Button title="Eliminar" onPress={ deleteProducto } color="red" />
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 15,
            }}>
            <Button
              title="Camara"
              onPress={ takePhoto }
              color="#5856D6"
            />
            <View style={{width: 10}} />
            <Button
              title="Galeria"
              onPress={() => console.log('Enviando crear producto')}
              color="#5856D6"
            />
          </View>
          </>
        )}

        {/* Imprimiendo la imagen */}
        {
        (img.length > 0 && !tempUri) && (
          <Image
            source={{uri: img}}
            style={{
              marginTop: 30,
              width: '100%',
              height: 300,
            }}
          />
        )}

        {/* Mostrar imagen temporal */}
        {
        (tempUri) && (
          <Image
            source={{uri: tempUri}}
            style={{
              marginTop: 30,
              width: '100%',
              height: 300,
            }}
          />
        )}

      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 20,
  },
  textInput: {
    marginTop: 9,
    marginBottom: 15,
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
  },
});
