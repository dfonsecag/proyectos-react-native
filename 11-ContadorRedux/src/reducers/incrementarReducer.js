import {
  INCREMENTANDO,
  INCREMENTANDO_EXITO,
  INCREMENTANDO_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  numero: 1,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case INCREMENTANDO:
      return {
        ...state,
        numero: state.numero + 1,
      };

    default:
      return state;
  }
}
