import { combineReducers } from "redux";

import incrementarReducer from "./incrementarReducer";

export default combineReducers({
  incremento: incrementarReducer
});