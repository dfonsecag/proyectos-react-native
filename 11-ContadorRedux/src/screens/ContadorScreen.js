import React, { useState } from 'react';
import {
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {useDispatch, useSelector} from "react-redux";
import { incrementar } from "../actions/contadorAction"

import { Background } from '../components/Background';
import { WhiteLogo } from '../components/WhiteLogo';
import { appStyles } from '../theme/loginTheme';




export const ContadorScreen = () => {

    const dispatch = useDispatch();

    const contar = () => dispatch(incrementar());

    const [contador, setContador] = useState(10);

    const numero = useSelector((state) => state.incremento.numero);
    console.log(numero)

    return (
          <>
      <Background />

      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <View style={appStyles.conetenedor}>
          <WhiteLogo />

          <Text style={appStyles.title}>Contador: {numero}</Text>

           {/* Boton login */}
           <View style={ appStyles.buttonContainer }>
                        <TouchableOpacity
                            activeOpacity={ 0.8 }
                            style={ appStyles.button }
                            onPress={ contar }
                        >
                            <Text style={ appStyles.buttonText } >Incrementar</Text>
                        </TouchableOpacity>
                    </View>
        </View>
      </KeyboardAvoidingView>
    </>
    )
}
