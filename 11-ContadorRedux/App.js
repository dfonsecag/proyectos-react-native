import React, { useState } from 'react';
import { Provider } from 'react-redux';

import store from "./store";




import { ContadorScreen } from './src/screens/ContadorScreen';

const App = () => {
  const [contador, setContador] = useState(10)
  return (
    <Provider store={store}>
       <ContadorScreen/>
    </Provider>
   
  );
};

export default App;
