import React, {useRef} from 'react';
import {Animated, Easing} from 'react-native';

export const useAnimation = () => {
  // Estableciendo opacidad de la caja
  const opacity = useRef(new Animated.Value(0)).current;

  const position = useRef(new Animated.Value(0)).current;

  // funcion que toma la opacidad actual y comienza hacer la animacion
  const fadeIn = ( duration:number = 3000) => {
    Animated.timing(opacity, {
      toValue: 1,
      duration:1500,
      useNativeDriver: true,
    }).start();
  };
  // funcion que le quita la opacidad
  const fadeOut = () => {
    Animated.timing(opacity, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  //
  const startMovingPosition = (initposition: number = -100, duration: number = 300) => {
    position.setValue(initposition);

    Animated.timing(position, {
      toValue: 0,
      duration: duration,
      useNativeDriver: true,
    //   easing: Easing.bounce,
    }).start();
  };

  return {
    opacity,
    position,
    fadeIn,
    fadeOut,
    startMovingPosition
  };
};
