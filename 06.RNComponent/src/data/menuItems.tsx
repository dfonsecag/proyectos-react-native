import { MenuItem } from "../interfaces/appInterfaces";

 export const menuItems:MenuItem[] = [
    {
        name: 'Animation 101',
        icon: 'cube-outline',
        component: 'Animation101Screen'
    },
    {
        name: 'Animation 102',
        icon: 'albums-outline',
        component: 'Animation102Screen'
    },
    {
        name: 'Switches',
        icon: 'toggle-outline',
        component: 'SwitchScreen'
    },
    {
        name: 'Alertas',
        icon: 'alert-circle-outline',
        component: 'AlertScreen'
    },
    {
        name: 'Text Input',
        icon: 'document-text-outline',
        component: 'TextInputScreen'
    },
    {
        name: 'Pagina Refrescar',
        icon: 'refresh-outline',
        component: 'PullToRefreshScreen'
    },
    {
        name: 'Sesion Lista',
        icon: 'list-outline',
        component: 'ListScreen'
    },
    {
        name: 'Modal',
        icon: 'copy-outline',
        component: 'ModalScreen'
    },
    {
        name: 'Scroll Infinite',
        icon: 'download-outline',
        component: 'InfinitScrollScreen'
    },
    {
        name: 'GPS Location',
        icon: 'locate-outline',
        component: 'GpsScreen'
    },
]