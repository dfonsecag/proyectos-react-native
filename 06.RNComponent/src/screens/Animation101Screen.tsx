import React, {useRef} from 'react';
import {Animated, Button, Easing, StyleSheet, View} from 'react-native';
import {useAnimation} from '../hooks/useAnimation';

export const Animation101Screen = () => {
  //   hooks Animated
  const {opacity, position, fadeIn, fadeOut, startMovingPosition} = useAnimation();

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          ...styles.purpleBox,
          opacity: opacity,
          transform: [{translateX: position}], //para iniciar de izquierda a derecha translateX
          marginBottom: 20,
        }}
      />

      <Button title="FadeIn" onPress={ () => { 
          fadeIn(); 
          startMovingPosition(-100, 1500);
          }} />

      <Button title="FadeOut" onPress={fadeOut} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  purpleBox: {
    backgroundColor: '#5856D6',
    width: 150,
    height: 150,
  },
});
