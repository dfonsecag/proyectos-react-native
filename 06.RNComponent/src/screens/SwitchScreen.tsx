import React, {useState} from 'react';
import {View, Switch, StyleSheet, Platform, Text} from 'react-native';
import {CustomSwitch} from '../components/CustomSwitch';
import {HeaderTitle} from '../components/HeaderTitle';

export const SwitchScreen = () => {
  const [state, setstate] = useState({
    isActive: true,
    isHungry: false,
    isHappy: true,
  });

  const {isActive, isHungry, isHappy} = state;

  const onChange = (value:boolean, field:string) => {
      setstate({
        ...state,
        [field]:value
      })
  }

  return (
    <View style={styles.container}>

      <HeaderTitle title="Switch" />

      <View style={styles.switchRow}>
        <Text style={styles.switchText}>Is Active</Text>
        <CustomSwitch isOn={isActive} onChange={(value) => onChange( value, 'isActive')}/>
      </View>

      <View style={styles.switchRow}>
        <Text style={styles.switchText}>is Hungry</Text>
        <CustomSwitch isOn={isHungry} onChange={(value) => onChange( value, 'isHungry')}/>
      </View>

      <View style={styles.switchRow}>
        <Text style={styles.switchText}>is Happy</Text>
        <CustomSwitch isOn={isHappy} onChange={(value) => onChange( value, 'isHappy')}/>
      </View>

     

      <Text style={styles.switchText}>{JSON.stringify(state, null, 5)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  switchText: {
    fontSize: 25,
  },
  switchRow:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginVertical:8
    
  }
});
