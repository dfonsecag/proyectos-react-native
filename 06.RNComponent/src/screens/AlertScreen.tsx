import React from 'react'
import { Alert, Button, View } from 'react-native'
import { HeaderTitle } from '../components/HeaderTitle'
import { styles } from '../theme/appTheme';

export const AlertScreen = () => {

    const showAlert = () => {
        Alert.alert(
            "Titulo",
            "Mensaje de la alerta",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
            // ,
            // {
            //     cancelable:true, //para si presiona afuera se cierra la alerta
            //     onDismiss: () => console.log('onDismiss')
            // }
        )
    }

    return (
        <View style={styles.globalMargin}>
            <HeaderTitle title="Alerta"/>

            <Button
                title="Mostrar Alerta"
                onPress={ showAlert }
            />

        </View>
    )
}
