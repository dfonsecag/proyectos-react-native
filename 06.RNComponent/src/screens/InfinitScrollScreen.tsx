import React from 'react'
import { Image, StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { HeaderTitle } from '../components/HeaderTitle';
import { useState } from 'react';
import { FadeInImage } from '../components/FadeInImage';

export const InfinitScrollScreen = () => {

    const [numbers, setNumbers] = useState([0,1,2]);

    const renderItem = (item:number) => {
        return(
            // <Image
            //     source={{uri:`https://picsum.photos/id/${ item }/500/400`}}
            //     style={{
            //         width: '100%',
            //         height: 400

            //     }}
            // />
            <FadeInImage uri={`https://picsum.photos/id/${ item }/500/400`}/>
        )
    }

    const loadMore = () => {
        
        const newArray:number[] = [];
        for (let i = 0; i < 3; i++) {
           newArray[i] = numbers.length + i;
        }

        setTimeout(() => {
            setNumbers([...numbers, ...newArray]);
        }, 1500);

       
    }

    return (
        <View style={{flex:1}}>
           

            <FlatList
                data={numbers}
                keyExtractor={(item) => item.toString() }
                renderItem={({item}) =>renderItem(item) }

                ListHeaderComponent={ <HeaderTitle title="Scroll Infinite"/>}

                onEndReached={ loadMore } // cuando llega al fondo de FlatList
                onEndReachedThreshold={ 0.1 }

                ListFooterComponent={ () => (
                    <View style={{height:80, justifyContent:'center', alignItems:'center'}}>
                        <ActivityIndicator size={25} color="#5856D6"/>
                    </View>
                )}


            />

        </View>
    )
}

const styleInfineScroll = StyleSheet.create({
    textItem:{
        backgroundColor:'rgba(0,0,0,0.2)',
        height: 150
    }
})