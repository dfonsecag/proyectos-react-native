import React from 'react'
import { FlatList, Text, View } from 'react-native';
import { FlagListMenuItem } from '../components/FlagListMenuItem';
import { HeaderTitle } from '../components/HeaderTitle';
import { ItemSeparator } from '../components/ItemSeparator';
import { menuItems } from '../data/menuItems';
import { styles } from '../theme/appTheme';





export const HomeScreen = () => {




    return (
        <View style={{flex:1,...styles.globalMargin}}>
         
          <FlatList
          
            data={menuItems}
            renderItem={({item}) => <FlagListMenuItem menuItem={item}/>}
            keyExtractor={ (item) => item.name}  //Debe ser string KeyExtractor
            ListHeaderComponent={ <HeaderTitle title="Opciones del menu"/> } //cabezado de la lista
            ItemSeparatorComponent={()=> <ItemSeparator/>}//Separador

          />
        </View>
    )
}
