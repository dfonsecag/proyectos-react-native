import React, {useState} from 'react';
import {Text, View, ScrollView, RefreshControl} from 'react-native';
import {} from 'react-native-gesture-handler';
import {HeaderTitle} from '../components/HeaderTitle';

export const PullToRefreshScreen = () => {
  const [refreshing, setRefreshing] = useState(false);

  const [data, setData] = useState<String>();

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      console.log('Terminamos');
      setRefreshing(false);
      setData('Hola mundo')
    }, 1500);
    
  };

  return (
    <ScrollView
      refreshControl={
        <RefreshControl 
            refreshing={refreshing}
            onRefresh={onRefresh}
            progressViewOffset= { 10 }
            // progressBackgroundColor="#5856D6"
            // colors= { ['white', 'red', 'orange'] } 
        />
      }>
      <View>
        <HeaderTitle title="Pagina Refrescar" />

        <Text>{data}</Text>

      </View>
    </ScrollView>
  );
};
