import React, {useState} from 'react';
import {Button, Modal, StyleSheet, Text, View} from 'react-native';
import {HeaderTitle} from '../components/HeaderTitle';
import {styles} from '../theme/appTheme';

export const ModalScreen = () => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <View style={styles.globalMargin}>
      <HeaderTitle title="Modal" />

      <Button title="Abrir Modal" onPress={() => setIsVisible(true)} />

      <Modal animationType="slide" visible={isVisible} transparent>
        {/* Modal Negro claro */}
        <View style={styleModal.modal}>
          {/* Contenido del modal */}
          <View style={styleModal.cuerpoModal}>
            <Text style={{fontSize:20, fontWeight:'bold'}}>Modal</Text>
            <Text style={{fontSize:16, fontWeight:'300', marginBottom:20, marginTop:15}}>Cuerpo del Modal</Text>
            <Button title="Cerrar" onPress={() => setIsVisible(false)} />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styleModal = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    // width: 100,
    // height: 100
    justifyContent: 'center',
    alignItems: 'center',
  },
  cuerpoModal: {
    backgroundColor: 'white',
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset:{
        width: 0,
        height: 10
    },
    shadowOpacity:0.25,
    elevation:10,
    borderRadius:8
  },
});
