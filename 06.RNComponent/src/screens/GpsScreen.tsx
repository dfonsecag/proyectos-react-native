import React from 'react';
import GetLocation from 'react-native-get-location';
import {Alert, Button, Text, View} from 'react-native';
import {HeaderTitle} from '../components/HeaderTitle';

export const GpsScreen = () => {
  // Funcion para obtener la ubicacion del usuario
  const ubicacion = async () => {
    try {
      const gps = await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
      });
      Alert.alert(
        'La ubicacion Actual',
        `La latitud es: ${gps.latitude} y la longitud es: ${gps.longitude}`,
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
      );
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View>
      <HeaderTitle title="GPS Localizacion" />     

      <Button title="Imprimir ubicacion" onPress={ubicacion} />

    </View>
  );
};
