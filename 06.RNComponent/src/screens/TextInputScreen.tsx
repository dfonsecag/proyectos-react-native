import React, {useState} from 'react';
import {StyleSheet, TextInput, View, KeyboardAvoidingView, Platform, Text} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { CustomSwitch } from '../components/CustomSwitch';
import {HeaderTitle} from '../components/HeaderTitle';
import { useForm } from '../hooks/useForm';
import {styles} from '../theme/appTheme';

export const TextInputScreen = () => {

  const {form, onChange, isSubscribed} = useForm({
    name: '',
    email: '',
    phone: '',
    isSubscribed:false
  })



  return (
    <KeyboardAvoidingView
    behavior={Platform.OS === "ios" ? "padding" : "height"}
    >

    <ScrollView>

      <View style={styles.globalMargin}>
        <HeaderTitle title="Text Input" />

        <TextInput
          style={stylesTextInput.input}
          placeholder="Ingrese su nombre"
          autoCorrect={false}
          autoCapitalize="words"
          onChangeText={value => onChange(value, 'name')}
        />

        <TextInput
          style={stylesTextInput.input}
          placeholder="Ingrese su email"
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={value => onChange(value, 'email')}
          keyboardType="email-address"
        />
        {/* Suscribirme */}
        <Text>Suscribirme</Text>
        <View style={styles.switchRow}>
        <Text style={styles.switchText}>Subscribirse</Text>
        <CustomSwitch isOn={isSubscribed} onChange={(value) => onChange( value, 'isSubscribed')}/>
      </View>

        <HeaderTitle title={JSON.stringify(form, null, 2)} />
        <HeaderTitle title={JSON.stringify(form, null, 2)} />
        <TextInput
          style={stylesTextInput.input}
          placeholder="Ingrese su telefono"
          onChangeText={value => onChange(value, 'phone')}
          keyboardType="phone-pad"
        />
      </View>

      <View style={{height:100}}/>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const stylesTextInput = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.8)',
    // opacity: 0.2,
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
});
