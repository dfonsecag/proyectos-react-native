import React from 'react'
import { SectionList, Text, View } from 'react-native'
import { HeaderTitle } from '../components/HeaderTitle'
import { ItemSeparator } from '../components/ItemSeparator';
import { styles } from '../theme/appTheme';

interface Casas {
    casa: string;
    data: string[];
}

const casas: Casas[] = [
    {
      casa: "DC Comics",
      data: ["Batman", "Superman", "Robin","Batman", "Superman", "Robin", ]
    },
    {
      casa: "Marvel Comics",
      data: ["Antman", "Thor", "Spiderman","Antman","Antman", "Thor", "Spiderman","Antman","Antman", "Thor", "Spiderman","Antman", ]
    },
    {
      casa: "Anime",
      data: ["Kenshin", "Goku", "Saitama","Kenshin", "Goku", "Saitama","Kenshin", "Goku", "Saitama","Kenshin", "Goku", "Saitama",  ]
    }
];

export const ListScreen = () => {
    return (
        <View style={{...styles.globalMargin, flex:1}}>

     

             <SectionList
                sections={ casas }
                renderItem={( {item, index})  => <Text>{ item }</Text>}
                keyExtractor={ (item, index) => item + index}

                ListHeaderComponent={() =>    <HeaderTitle title="Header Sesion" />} // Header de la pagina
                ListFooterComponent={() =>  <HeaderTitle title={`Total de casas: ${casas.length}`} />}

                stickySectionHeadersEnabled={true}// para que el header list no desaparezca
                renderSectionHeader= { ({section }) => (
                    <View style={{backgroundColor:'white'}}>
                        <HeaderTitle title={section.casa} />
                    </View>
                ) }

                renderSectionFooter={({ section}) => (
                    <HeaderTitle title={`Total: ${section.data.length}`} />
                )}
                
                // SectionSeparatorComponent={() =>  <ItemSeparator/>}
                ItemSeparatorComponent={() =>  <ItemSeparator/>}

                showsVerticalScrollIndicator={false}

             />
        </View>
    )
}
