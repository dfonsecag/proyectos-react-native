import React from 'react';
import {Text, View} from 'react-native';
import {BotonCalc} from '../components/BotonCalc';
import {styles} from '../theme/appTheme';
import {useCalculadora} from '../hooks/useCalculadora';

export const CalculadoraScreen = () => {
  const {
    numeroAnterior,
    numero,
    limpiar,
    positivoNegativo,
    btnDelete,
    botonDividir,
    armarNumero,
    botonMultiplicar,
    botonRestar,
    botonSumar,
    calcular,
  } = useCalculadora();

  return (
    <View style={styles.calculadoraContainer}>
      {numeroAnterior !== '0' && (
        <Text style={styles.resultadopequeno}>{numeroAnterior}</Text>
      )}

      <Text
        style={styles.resultado}
        numberOfLines={1}
        adjustsFontSizeToFit={true}>
        {numero}
      </Text>

      {/* Fila de botones */}
      <View style={styles.fila}>
        <BotonCalc texto="C" color="#9B9B9B" accion={limpiar}></BotonCalc>
        <BotonCalc
          texto="+/-"
          color="#9B9B9B"
          accion={positivoNegativo}></BotonCalc>
        <BotonCalc texto="del" color="#9B9B9B" accion={btnDelete}></BotonCalc>
        <BotonCalc texto="/" color="#FF9427" accion={botonDividir}></BotonCalc>
      </View>

      {/* Fila de botones */}
      <View style={styles.fila}>
        <BotonCalc texto="7" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="8" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="9" accion={armarNumero}></BotonCalc>
        <BotonCalc
          texto="x"
          color="#FF9427"
          accion={botonMultiplicar}></BotonCalc>
      </View>

      {/* Fila de botones */}
      <View style={styles.fila}>
        <BotonCalc texto="4" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="5" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="6" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="-" color="#FF9427" accion={botonRestar}></BotonCalc>
      </View>

      {/* Fila de botones */}
      <View style={styles.fila}>
        <BotonCalc texto="1" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="2" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="3" accion={armarNumero}></BotonCalc>
        <BotonCalc texto="+" color="#FF9427" accion={botonSumar}></BotonCalc>
      </View>

      {/* Fila de botones */}
      <View style={styles.fila}>
        <BotonCalc texto="0" ancho accion={armarNumero}></BotonCalc>
        <BotonCalc texto="." accion={armarNumero}></BotonCalc>
        <BotonCalc texto="=" color="#FF9427" accion={calcular}></BotonCalc>
      </View>
    </View>
  );
};
