import React, { useState } from 'react';
import { Provider } from 'react-redux';

import store from "./store";




import { ClimaScreen } from './src/screens/ClimaScreen';

const App = () => {
  const [contador, setContador] = useState(10)
  return (
    <Provider store={store}>
       <ClimaScreen/>
    </Provider>
   
  );
};

export default App;
