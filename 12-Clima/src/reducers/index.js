import { combineReducers } from "redux";

import incrementarReducer from "./incrementarReducer";
import climaReducer from "./climaReducer";

export default combineReducers({
  incremento: incrementarReducer,
  clima: climaReducer
});