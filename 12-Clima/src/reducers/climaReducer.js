import {
  COMENZAR_CONSULTA,
  COMENZAR_CONSULTA_EXITO,
  COMENZAR_CONSULTA_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  loading: false,
  error: false,
  clima: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case COMENZAR_CONSULTA:
      return {
        ...state,
        clima: {},
        loading:true
      };
    case COMENZAR_CONSULTA_EXITO:
      return {
        ...state,
        loading:false,
        error:false,
        clima: action.payload,
      };
    case COMENZAR_CONSULTA_ERROR:
      return {
        ...state,
        loading:false,
        error:true,
      };

    default:
      return state;
  }
}
