import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {ActivityIndicator, Alert, Text, View} from 'react-native';
import {appStyles} from '../theme/loginTheme';

export const Clima = () => {
  const {name, main} = useSelector(state => state.clima.clima);
  const cargando = useSelector(state => state.clima.loading);
  const error = useSelector(state => state.clima.error);

  let componente;
  if (name && !cargando) {
    componente = (
      <>
        <Text style={appStyles.infoClima}>El clima de {name} es:</Text>
        <Text style={{...appStyles.infoClima, fontSize: 50}}>
          {parseFloat(main.temp - 273.15, 10).toFixed(2)} &#x2103;
        </Text>
        <Text style={appStyles.infoClima}>
          Temperatura Minima {parseFloat(main.temp_min - 273.15, 10).toFixed(2)}{' '}
          &#x2103;
        </Text>
        <Text style={appStyles.infoClima}>
          Temperatura Maxima {parseFloat(main.temp_max - 273.15, 10).toFixed(2)}{' '}
          &#x2103;
        </Text>
        <Text style={appStyles.infoClima}>
          La humedad es de: {main.humidity}
        </Text>
      </>
    );
  } else if (error) {
    componente = <Text style={appStyles.infoClima}>No hay Resultados</Text>;
  } else if (cargando) {
    componente = (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size={50} color="black" />
      </View>
    );
  }

  return <View style={{flex: 3}}>{componente}</View>;
};
