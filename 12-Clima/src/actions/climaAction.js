import axios from 'axios';
import {
  COMENZAR_CONSULTA,
  COMENZAR_CONSULTA_EXITO,
  COMENZAR_CONSULTA_ERROR,
} from '../types';

// CREAR NUEVO PRODUCTOS
export function consultarClima(pais, ciudad) {
  let datos = {};
  return async dispatch => {
    dispatch(comenzarConsultaClima());
    try {
      const resp = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=63d292defd1fdce03ed534c5084c7137`,
      );
      datos.name = resp.data.name;
      datos.main = resp.data.main;
      dispatch(comenzarConsultaClimaExito(datos));
    } catch (error) {
      console.log(error);
      dispatch(comenzarConsultaClimaError());
    }
  };
}
const comenzarConsultaClima = () => ({
  type: COMENZAR_CONSULTA,
});
const comenzarConsultaClimaExito = datos => ({
  type: COMENZAR_CONSULTA_EXITO,
  payload: datos,
});
const comenzarConsultaClimaError = () => ({
  type: COMENZAR_CONSULTA_ERROR,
});
