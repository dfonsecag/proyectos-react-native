import {Picker} from '@react-native-picker/picker';
import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  View,
} from 'react-native';

import {useDispatch} from 'react-redux';
import {consultarClima} from '../actions/climaAction';

import {Background} from '../components/Background';
import {Clima} from '../components/Clima';
import {appStyles} from '../theme/loginTheme';

export const ClimaScreen = () => {
  const [pais, setPais] = useState();
  const [ciudad, setCiudad] = useState('');

  const dispatch = useDispatch();

  const getconsultarClima = () => dispatch(consultarClima(pais, ciudad));





  return (
    <>
      <Background />

      <View
        style={{
          flex: 1,
          backgroundColor: '#5856D6',
        }}>
        <Text style={appStyles.title}>CLIMA REACT-NATIVE</Text>
      </View>

     

      <View style={appStyles.conetenedor}>
      <ScrollView>
        <Text style={appStyles.label}>Ciudad:</Text>
        <TextInput
          placeholderTextColor="rgba(255,255,255,0.4)"
          underlineColorAndroid="white"
          selectionColor="white"
          onChangeText={ciudad => setCiudad(ciudad)}
          defaultValue={ciudad}
          autoCapitalize="none"
          autoCorrect={false}
        />
        <Text style={appStyles.label}>Pais:</Text>

        <Picker
          selectedValue={pais}
          onValueChange={itemValue => setPais(itemValue)}>
          <Picker.Item label="Costa Rica" value="CR" />
          <Picker.Item label="Estados Unidos" value="US" />
          <Picker.Item label="México" value="MX" />
          <Picker.Item label="Argentina" value="AR" />
          <Picker.Item label="España" value="ES" />
          <Picker.Item label="Perú" value="PE" />
        </Picker>

        {/* Boton  */}
        <View style={appStyles.buttonContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{...appStyles.button, backgroundColor: 'green'}}
            onPress={getconsultarClima}>
            <Text style={appStyles.buttonText}>Buscar Clima</Text>
          </TouchableOpacity>
        </View>
        </ScrollView>
      </View>

      <Clima />

    </>
  );
};
